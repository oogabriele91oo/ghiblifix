import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import FilmCard from '../film-card/FilmCard';
import Banner from '../../assets/topImage.png';


export default function HomePage() {
  const [searchText, setSearchText] = useState('');
  const films = useSelector((state) => state.logic);
  const filteredFilms = films.filter((item) => Object.keys(item).some((key) => item[key]
    .toString()
    .toLowerCase()
    .includes(searchText.toLocaleLowerCase())));

  return (
    <div className="HomePage">
      <div className="BannerDiv">
        <Link to={"/"}>
          <img src={Banner} alt="Logo" className="GhibliLogo" />
        </Link>
      </div>
      <div className="InputContainer">
        <input value={searchText} onChange={(e) => setSearchText(e.target.value)} type="text" placeholder="Search films..." className="SearchInput" />
      </div>
      <div className="FilmsContainer">
        {
          filteredFilms.map((film) => (
            <FilmCard
              key={film.filmKey}
              filmKey={film.filmKey}
              title={film.title}
              release={film.release}
              image={film.image}
            />
          ))
        }
      </div>
    </div>
  );
}
