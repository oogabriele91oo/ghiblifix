import { Link } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleLeft, faUser, faGear } from '@fortawesome/free-solid-svg-icons';

export default function Header() {
  return (
    <header className="Header">
      <nav className="Navbar">
        <Link to="/">
          <FontAwesomeIcon icon={faAngleLeft} className="Icon FirstIcon" />
        </Link>
        <h2>NETFLIX - 株式会社スタジオジブリ</h2>
        <div className="IconContainer">
          <FontAwesomeIcon icon={faUser} className="Icon" />
          <FontAwesomeIcon icon={faGear} className="Icon LastIcon" />
        </div>
      </nav>
    </header>
  );
}

