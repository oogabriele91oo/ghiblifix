import { Routes, Route } from 'react-router-dom';
import Header from './components/header/Header';
import './assets/styles/App.css';
import 'font-awesome/css/font-awesome.min.css';
import HomePage from './components/home-page/HomePage';
import FilmDetails from './components/film-details/FilmDetails';

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/FilmDetails/:id" element={<FilmDetails />} />
      </Routes>
    </div>
  );
}

export default App;
