import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import logic from './reduces/logic';
import './assets/styles/index.css';
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Provider logic={logic}>
        <App />
      </Provider>
    </Router>
  </React.StrictMode>,
  document.getElementById('root'),
);
